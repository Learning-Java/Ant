# Ant
## About
A game about an Ant eating apples to win.

## Download & Use
To open and play it, you must have [Greenfoot](https://www.greenfoot.org/download) installed. <br>
The newest release of the game can be downloaded [here](https://codeberg.org/Learning-Java/Ant/releases).