import greenfoot.*; // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Code for the ant.
 * 
 * @author MagicLike 
 * @version 0.1
 */
public class Ant extends Actor
{
    public Ant()
    {
        setRotation(270);   // Sets the standard rotation
    }
    public void act()   // Calls all methods in script
    {
        move();
        snack();
        update();
    }
    public void move()  // Key controls for ant
    {
        int mS = 5; // Speed variable [cells]
        
        int x = getX(); // Get x-coordinates
        int y = getY(); // Get y-coordinates
        
        // WASD controls
        if (Greenfoot.isKeyDown("w"))
        {
        setRotation(270);
        setLocation (x,y-mS);
        }
        else if (Greenfoot.isKeyDown("a"))
        {
        setRotation(180);
        setLocation(x-mS,y);
        }
        else if (Greenfoot.isKeyDown("s"))
        {
        setRotation(90);
        setLocation(x,y+mS);
        }
        else if (Greenfoot.isKeyDown("d"))
        {
        setRotation(0);
        setLocation(x+mS,y);
        }
        
        // Dance mode
        else if (Greenfoot.isKeyDown("x"))
        {
            move (20);
            turn (90);
            move (50);
        }
    }
    public void snack() // Snacking apples...
    {
        if(isTouching(Apple.class))
        {
            removeTouching(Apple.class);
            ((AntWorld)getWorld()).setCount();
        }
    }
    public void update()    // Updates elements in the world
    {
        ((AntWorld)getWorld()).snackCounter();
        ((AntWorld)getWorld()).endScreen();
    }
}
