import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * The world of the game "Ant".
 * 
 * @author MagicLike
 * @version 0.1
 */
public class AntWorld extends World
{
    private int snackLimit = 10; // Variable for the number of apples
    private int snackCount; // Counting how many apples have been snacked
    /**
     * Constructor for objects of class AntWorld.
     */
    public AntWorld()
    {    
        // Create a new world with 1000x800 cells with a cell size of 1x1 pixels.
        super(1000, 800, 1);
        
        // Spawn the ant
        Ant ant1 = new Ant ();  // Class Name = new Constructor
        addObject (ant1, 500, 400); // Place ant in the middle of world
        
        // Add math to place ant in the middle of the world -> vars for size of world /2 = vars for middle of world
        
        // Apples - spawning apples at random locations
        for(int i=0; i<snackLimit; i++){
        int x = Greenfoot.getRandomNumber(getWidth());  // Generate random x-coordinate
        int y = Greenfoot.getRandomNumber(getHeight()); // Generate random y-coordinate
        
        Apple apple1 = new Apple ();    // Class Name = new Constructor
        addObject (apple1, x, y);
        }
        
        showText("Control the ant with WASD and eat all apples to win!",750,780);   // Show how to play the game
        
        snackCounter(); // Calls the counter method at construction
    }
    public void setCount()  // Add +1 to the snack counter
    {
        snackCount++;
    }
    public void snackCounter()  // Show the snack counter
    {
        showText("Snacked apples:"+snackCount,90,15);
    }
    public void endScreen() // Showing, when the player has won the game
    {
        if(snackCount==snackLimit)
        {
            showText("You won!",500,400);   // Print out "You won!"
            showText("Score:"+snackCount,500,430);  // Print out the score, the player reached
            Greenfoot.stop();   // Stop the game -> The player must restart the game to play again
        }
    }
}
